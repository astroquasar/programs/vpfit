# VPFIT: A non-linear least square Voigt profile fitting program

A comprehensive list of publications that used VPFIT in their work can be found through the Astrophysics Data System (ADS) database. [here](https://ui.adsabs.harvard.edu/#search/q=%20full%3A%22vpfit%22&sort=date%20desc%2C%20bibcode%20desc) is the direct link to the list. The full bibtex record of all the publications (generated from ADS), is available [here](https://github.com/VPFIT/vpfit/blob/master/vpfit.bib).

## VPFIT versions

### Version Access

Each version is placed into separate branch and can be loaded easily by moving to the corresponding branch using the `git checkout` command, e.g. as follows:

```
git checkout mvpfit0.3
```

We are listing below  all the VPFIT versions available in this repository:
- [mvpfit0.3](https://gitlab.com/astroquasar/programs/vpfit/tree/mvpfit0.3)
- [vpfit10](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit10)
- [vpfit10.2](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit10.2)
- [vpfit10.4](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit10.4)
- [vpfit10x](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit10x)
- [vpfit3](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit3)
- [vpfit5](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit5)
- [vpfit8](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit8)
- [vpfit9.1](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.1)
- [vpfit9.1dev](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.1dev)
- [vpfit9.2Feb20](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.2Feb20)
- [vpfit9.2dev](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.2dev)
- [vpfit9.3beta](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.3beta)
- [vpfit9.4](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.4)
- [vpfit9.5](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit9.5)
- [vpfit_17_03_04](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit_17_03_04)
- [vpfit_1994](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit_1994)
- [vpfit_9.5_jk_2010-06-26](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit_9.5_jk_2010-06-26)
- [vpfit_ascii](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit_ascii)
- [vpfit_v8.02](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfit_v8.02)
- [vpfitdev](https://gitlab.com/astroquasar/programs/vpfit/tree/vpfitdev)

### Major changes

Below is a table from [Vincent Dumont's PhD thesis](https://www.unsworks.unsw.edu.au/permalink/f/5gm2j3/unsworks_51882) listing the vpfit versions in which major changes from previous versions took place - in particular, changes which may impact on the precision with which ∆α/α is measured. Versions which are not included in this Table contained minor updates or updates that do not impact on varying alpha measurements. The changes briefly outlined here were not carried out as part of the research described in this Thesis. The changes were carried out by Bob Carswell and John Webb.

<img src="versions.png" alt="versions" width="70%"/>

## Troubleshooting

### Install PGPLOT on Mac

Once MacPorts is available, you can trivially install many packages, in particular PGPLOT (which in addition requires xorg-server): 

```
sudo port install xorg-server
sudo port install pgplot
```

In order to use MacPorts' X11 as the default server, you need to log out, and then log back in. After the installation, make sure that the expected environment variables are properly set (at least `PGPLOT_DIR` must exist; `PGPLOT_DEV` is also quite useful):

```
export PGPLOT_DIR=/opt/local/lib
export PGPLOT_DEV=/Xserve
```

### Notes from John Webb

Notes on trying to sort out pgplot, gcc, and other problems (11 Oct 2017 Cabangan). This is what I did:

```
sudo port selfupdate
sudo port upgrade outdated
```

During this, the following message was repeated multiple times:

```
Warning: The Xcode Command Line Tools don't appear to be installed; most ports will likely fail to build.
Warning: Install them by running `xcode-select --install'.
```

I then tried to do a `xcode-select --install` but it was slow and estimated "about an hour" so I did not proceed. I then did a `sudo port install gcc7` which seemed to work although had the same complaint as above.

### Installing VPFIT on macOS Mojave

I was installing VPFIT 10.4 on my recently upgraded Macbook to OS 10.14 Mojave. I installed pgplot and cfitsio through MacPorts and run make vpmac but got the folllowing error:

```
Undefined symbols for architecture x86_64:
 "_ftclos_", referenced from:
     _vp_rdwaveval_ in vp_rdwaveval.o
     _vp_readfits_ in vp_readfits.o
 …
 "_ftopen_", referenced from:
     _vp_rdwaveval_ in vp_rdwaveval.o
     _vp_readfits_ in vp_readfits.o
ld: symbol(s) not found for architecture x86_64
collect2: error: ld returned 1 exit status
make: *** [vpmac] Error 1
```

Functions such as ftclos are from the cfitsio library, I managed to fix those errors by installing cfitsio from the Github repository. However, new errors occurred with the curl library:

```
Undefined symbols for architecture x86_64:
  "_curl_easy_cleanup", referenced from:
      _https_open_network in libcfitsio.a(drvrnet.o)
  "_curl_easy_init", referenced from:
      _https_open_network in libcfitsio.a(drvrnet.o)
  "_curl_easy_perform", referenced from:
      _https_open_network in libcfitsio.a(drvrnet.o)
  "_curl_easy_setopt", referenced from:
      _https_open_network in libcfitsio.a(drvrnet.o)
  "_curl_global_cleanup", referenced from:
      _ffchtps in libcfitsio.a(cfileio.o)
  "_curl_global_init", referenced from:
      _ffihtps in libcfitsio.a(cfileio.o)
ld: symbol(s) not found for architecture x86_64
collect2: error: ld returned 1 exit status
make: *** [vpmac] Error 1
```

That is actually easy to fix as we only need to link the curl library within the makefile of VPFIT using the command `-lcurl` as mentioned in [this discussion](https://stackoverflow.com/questions/3840957/lib-curl-symbol-not-found).
