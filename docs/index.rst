.. title:: Docs
		      
VPFIT Knowledge Base
====================

This documentation aims to provide detailed information regarding the VPFIT program, including the theory behind the algorithm, the modifications throughout the released versions and tutorials on how to properly build Voigt profile models. The principal maintainer of the software is `Prof. Robert Carswell <https://www.ast.cam.ac.uk/~rfc/>`_ from the Institute of Astronomy at Cambridge University. The main website of the software is accessible via `this link <https://www.ast.cam.ac.uk/~rfc/vpfit.html>`_. Our `VPFIT GitLab repository <https://gitlab.com/astroquasar/programs/qscan>`_ gathers all the previous versions.

Getting Started
---------------

.. toctree::
   :maxdepth: 1

   background
   installation
   tutorial

Software Updates
----------------
   
.. toctree::
   :maxdepth: 1
   
   troubleshooting
   releases
