Background
==========

This program was made to manually identify absorption systems in quasar spectra. In this page, I hope to provide sufficient information regarding quasar spectrum features to help users to understand their data.

Quasar spectra
--------------

.. image:: _images/quasar_spectrum.jpg
   :align: center
   :width: 100%
